package com.example.mirroring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MirroringApplication {

    public static void main(String[] args) {
        SpringApplication.run(MirroringApplication.class, args);
    }

}
