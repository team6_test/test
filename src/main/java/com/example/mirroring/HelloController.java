package com.example.mirroring;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/hello")
public class HelloController {

    private final HelloService helloService;

    @GetMapping("")
    public String hello() {
        return "hello";
    }
}
